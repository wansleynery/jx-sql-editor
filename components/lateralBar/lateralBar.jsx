class LateralBar extends React.Component {

    constructor (props) {
        super (props);

        this.state = {
            actualTab: 'ENT',
            queriesHistoric: [],
            entities: null
        };

        this.setTabStatus = this.setTabStatus.bind (this);
    }

    componentDidMount () {
        this.getEntities ();
        setTimeout (() => updateTree (), 1000);
        setTimeout (() => this.updateHistoric (), 1000);

        /** globalThis.refreshHistoric = list => this.setState ({ queriesHistoric: list }); */
    }

    render () {
        
        switch (this.state.actualTab) {
            case 'HIS':
                return (
                    <div id="editorContainerReact">
                        <BarController setTabStatus= { this.setTabStatus } selected= { this.state.actualTab } />
        
                        <div id= { 'historicUpperContainer' } >
                            {
                                this.state.queriesHistoric.length &&
                                this.state.queriesHistoric.
                                    filter (query => query.ATIVO === 'S').
                                    map (query => (
        
                                        <EditorViewer
                                            key     = { query.ID }
                                            user    = { `USUÁRIO ${ query.IDUSUARIO }` }
                                            created = { query.DATA.fromSQLToDate ().toSQLTimestamp () }
                                            query   = { query.COMANDO }
                                            remover = { () => this.removeElement (query.ID) }
                                            plugins = { ['line-numbers', 'remove-initial-line-feed', 'remove-trailing', 'remove-indent', 'left-trim'] }
                                        />
            
                                    ))
                            }
                        </div>
                    </div>
                );
        
            case 'ENT': {
                return (
                    <div id="editorContainerReact">
                        <BarController setTabStatus= { this.setTabStatus } selected= { this.state.actualTab } />
        
                        <div id= { 'entitiesUpperContainer' } >
                            {
                                this.state.entities && this.state.entities.length &&
                                <TreeTrunk trunks= { this.state.entities } />
                            }
                        </div>
                    </div>
                );
            }

            case 'CON': {
                return (
                    <div id="editorContainerReact">
                        <BarController setTabStatus= { this.setTabStatus } selected= { this.state.actualTab } />
        
                        <div id= { 'historicUpperContainer' } >
                        </div>
                    </div>
                );
            }
        }
    }

    /** Updates the historic with the historic table records */
    updateHistoric () {

        // Recover the Query Manager
        query.
            // Executes a new query
            select ('SELECT * FROM AD_TJXHIS ORDER BY DATA DESC').
            then (response => {
                // If there is a response with records inside
                if (response && response.length) {
                    // Rebuilds the historic array
                    this.setState ({ queriesHistoric: [ ...response ] });

                }
            });
    }

    /** Hides a record from the historic table */
    removeElement (id) {

        this.setState ({
            queriesHistoric: this.state.queriesHistoric.map (q => {
                if (q.ID === id)
                    q.ATIVO = 'N';

                return q;
            }).filter (q => query.ATIVO === 'S')
        });

    }

    /** Changes the tab to show */
    setTabStatus (newStatus) {
        this.setState ({
            actualTab: newStatus
        });
    }

    async getEntities () {
        let data = await fetch (`${ globalLocal }/addons/entities.json`).
            then (x => x.text ()).
            then (y => y);

        this.setState ({ entities: JSON.parse (data) });
    }
}