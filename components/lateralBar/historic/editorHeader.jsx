class EditorHeader extends React.Component {

    constructor (props) {
        super (props);

        this.editQuery = this.editQuery.bind (this);
    }

    render () {

        return (
            <div className="editorHeader">

                <p> { this.props.user } - { this.props.created } </p>

                <button className = "editorHeaderButton close" onClick = { this.props.remover }> Remover </button>

                <button className = "editorHeaderButton edit" onClick = { this.editQuery }> Editar </button>

            </div>
        );

    }

    editQuery () {
        editor.getSession ().setValue (this.props.query);
        setFocus ();
    }

}