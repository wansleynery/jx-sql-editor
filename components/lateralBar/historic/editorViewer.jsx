class EditorViewer extends React.Component {
    constructor (props) {
        super (props);

        this.state = {
            content: this.props.query.trim ()
        }

        this.ref = React.createRef ();
    }

    componentDidMount () {
        setTimeout (() => Prism.highlightAll (), 0);
    }

    render () {
        return (
            <div className = "historicContainer">
                <pre className = { !this.props.plugins ? '' : this.props.plugins.join (' ') } data-toolbar-order = "">

                    <EditorHeader
                        user    = { this.props.user }
                        query   = { this.props.query }
                        created = { this.props.created }
                        remover = { this.props.remover }
                    />

                    <code ref = { this.ref } className = 'language-sql'>
                        { this.state.content }
                    </code>

                </pre>
            </div>
        );
    }
}