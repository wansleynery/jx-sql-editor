class BarTab extends React.Component {

    constructor (props) {
        super (props);
    }

    render () {
        return (
            <div
                id = { 'bar-tab' }
                className = { `${ this.props.colorClass } ${ this.props.activeClass }` }
                onClick = { this.props.clickHandler }
            >
                <span>
                    { this.props.name }
                </span>
            </div>
        );
    }
}