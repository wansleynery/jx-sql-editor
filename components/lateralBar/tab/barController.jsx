class BarController extends React.Component {

    constructor (props) {
        super (props);

        this.state = {
            buttons: [
                {
                    id: 0,
                    value: 'HIS',
                    name: 'Histórico',
                    colorClass: ''
                },
                {
                    id: 1,
                    value: 'ENT',
                    name: 'Entidades',
                    colorClass: ''
                },
                {
                    id: 2,
                    value: 'CON',
                    name: 'Configurações',
                    colorClass: ''
                }
            ]
        }
    }

    render () {
        return (
            <div id = { 'barController' } >

                {
                    this.state.buttons.map ((button, index) =>

                        <BarTab
                            key= { index }
                            name = { button.name }
                            colorClass = { button.colorClass }
                            activeClass = { button.value === this.props.selected ? 'active' : '' }
                            clickHandler = { () => this.activateButton (button.value) }
                        />

                    )
                }

            </div>
        );
    }

    activateButton (value) {
        this.props.setTabStatus (value);
        showLateralBarComponent ();
    }
}