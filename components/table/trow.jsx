class TRow extends React.Component {

    constructor (props) {
        super (props);
    }

    render () {
        return (
            <tr className= { 'queryTableBodyRow' } >
                {
                    Object.keys (this.props.column).map ((row, index) =>
                        <td key= { index }> { String (this.props.column [row]) } </td>
                    )
                }
            </tr>
        );
    }
}