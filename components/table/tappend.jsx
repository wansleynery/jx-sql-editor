class TAppend extends React.Component {

    constructor (props) {
        super (props);

        this.state = {
            expanded: true,
            formatted: false,
            oldSession: null
        }

        this.db = new DBProcessor (query);
    }

    render () {
        return (

            <div id= { 'tableAppend' } >

                <div onClick= { () => this.executeCommand () } ><i className= { 'fas fa-play fl' }></i></div>

                <i className= { 'fas fa-save fl' }></i>

                <div onClick= { () => autoFormat () } ><i className= { 'fas fa-align-right fl' }></i></div>

                <i className= { 'fas fa-table fr' }></i>

                <div onClick= { () => this.hideTable () } ><i className= { 'fas fa-chevron-down fr' }></i></div>

                <div onClick= { () => this.showTable () } ><i className= { 'fas fa-chevron-up fr' }></i></div>

            </div>

        );
    }

    hideTable () {
        this.setState ({
            expanded: false
        });

        hideTableComponent ();
    }

    showTable () {
        this.setState ({
            expanded: true
        });

        showTableComponent ();
    }

    executeCommand () {

        let comando = getSelectionText ();

        if (comando.length) {
            this.db.
                execute (comando).
                then (response => {

                    if (!this.db.isConsult (comando)) {
                        swal ({
                            title: 'Parabens!!',
                            text: 'Seu comando foi executado com sucesso!',
                            icon: 'success'
                        });
                    }
                    else {
                        globalThis.updateTable (response);
                        this.showTable ();
                    }

                    query.save ({
                        COMANDO : comando,
                        IDUSUARIO: 0,
                        DATA    : new Date ().toSQLDate (),
                        ATIVO   : 'S'
                    }, 'AD_TJXHIS').
                        then (() => {
                            query.select ('SELECT * FROM AD_TJXHIS ORDER BY DATA DESC').
                                then (newHistoric => globalThis.refreshHistoric (newHistoric));
                        });
                }).
                catch (error =>
                    swal ({
                        button: true,
                        closeOnClickOutside: true,
                        closeOnEsc: true,
                        title: 'Encontramos um problema...',
                        text: error.message,
                        icon: 'error'
                    })
                );
        } else {
            swal ({
                button: true,
                closeOnClickOutside: true,
                closeOnEsc: true,
                title: 'Cuidado...',
                text: 'Informe, ao menos, um comando pra gente executar!',
                icon: 'warning'
            });
        }
    }
}

ReactDOM.render (<TAppend />, document.querySelector ('#tableTop'));