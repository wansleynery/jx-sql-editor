class TBody extends React.Component {

    constructor (props) {
        super (props);
    }

    render () {

        this.props.data.length > 500 && swal ({ title: 'Atencao', text: 'Ha muitos registros retornados. Vamos mostrar apenas os primeiros 500 para te ajudar a se focar! ;)', icon: 'warning' });

        return (
            <tbody className= { 'queryTableBody' } >
                {
                    this.props.data.filter ((_value, index) => index < 500).map ((column, index) =>
                        <TRow key= { index } column= { column } />
                    )
                }
            </tbody>
        );
    }
}