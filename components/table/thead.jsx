class THead extends React.Component {

    constructor (props) {
        super (props);
    }

    render () {
        return (
            <thead className= { 'queryTableHead' } >
                <tr className= { 'queryTableHeaderRow' } >
                    {
                        this.props.headers.map ((column, index) =>
                            <th key= { index }> { column.toString () } </th>
                        )
                    }
                </tr>
            </thead>
        );
    }
}