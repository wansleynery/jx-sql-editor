class Table extends React.Component {

    constructor (props) {
        super (props);

        this.state = {
            headers : Object.keys (tableArray [0]),
            data    : tableArray,
            loading : false
        }

        globalThis.startLoading = () => this.setState ({ loading: true });
        globalThis.stopLoading = () => this.setState ({ loading: false });
    }

    componentDidMount () {
        let ps = new PerfectScrollbar (document.querySelector ('#table'));
        if ('.'.includes (',')) console.log (ps);

        globalThis.updateTable = newTable => {

            if (!newTable.length) {
                newTable = [
                    {
                        COLUNA: 'Não há valores a serem visualizados'
                    }
                ]
            }

            this.setState ({ data: newTable, headers: Object.keys (newTable [0]) })
        };
    }

    render () {
        return (
            <div>

                <LinearProgress variant= { this.state.loading ? 'indeterminate' : 'determinate' } value= { 100 } />

                <table className= { 'queryTable' } >
                    
                    <THead headers= { this.state.headers } />

                    <TBody data= { this.state.data } />

                </table>

            </div>
        );
    }
}

ReactDOM.render (<Table />, document.querySelector ('#table'));