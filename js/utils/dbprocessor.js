class DBProcessor {

    constructor (queryExecutor) {

        // Data Query Language - Linguagem de Consulta de dados
        this._DQL = ['SELECT', 'WITH'];

        // Data Manipulation Language - Linguagem de Manipulacao de Dados
        this._DML = ['INSERT', 'DELETE', 'UPDATE'];

        // Data Definition Language - Linguagem de Definicao de Dados
        this._DDL = ['CREATE', 'ALTER', 'DROP', 'TRUNCATE'];

        // Data Transaction Language - Linguagem de Transacao de Dados
        this._DTL = ['BEGIN TRANSACTION', 'COMMIT', 'ROLLBACK'];

        // Data Control Language - Linguagem de Controle de Dados
        this._DCL = ['GRANT', 'REVOKE', 'DENY'];

        this._query = queryExecutor;
    }

    isConsult (command) {
        return this._DQL.includes (command.trim ().match (/[a-z]+/gi) [0].toUpperCase ());
    }

    execute (fullCommand) {

        if (this.isConsult (fullCommand)) {
            return this._executeQueryCommand (fullCommand);
        }

        return this._executeSpecialCommand (fullCommand);

    }

    _executeQueryCommand (command) {
        return this._query.select (command);
    }

    async _executeSpecialCommand (command) {
        const actionResponse = await this._query.select (`SELECT IDBTNACAO AS ID FROM TSIBTA WHERE NOMEINSTANCIA = 'AD_TJXHIS' AND DESCRICAO LIKE '%Analisys'`);
        const resourceResponse = await this._query.select (`SELECT RESOURCEID AS RESID FROM TDDINS WHERE NOMETAB = 'AD_TJXHIS'`);

        return this._query.action ({ S: command }, { actionID: actionResponse [0].ID, resourceID: resourceResponse [0].RESID });
    }

}