if (document.getElementById ('editor')) {
    var editor = ace.edit ('editor');
    ace.config.set ('basePath', `${ globalLocal }/js/utils/ace`);
    editor.setTheme ('ace/theme/monokai');
    editor.getSession ().setUndoManager (
        editor.getSession ().getUndoManager ());

    editor.setAutoScrollEditorIntoView (true);
    editor.session.setTabSize (4);
    editor.session.setUseWrapMode (true);
    editor.session.setUseSoftTabs (true);
    editor.session.setMode ('ace/mode/sql');

    editor.setDisplayIndentGuides (true);
    editor.setWrapBehavioursEnabled (true);
    editor.setShowPrintMargin (false);
    editor.setShowInvisibles (true);

    editor.container.addEventListener ('contextmenu', function (e) {
        e.preventDefault ();
        return false;
    }, false);
}

var hideTableComponent = () => {
    if (document.getElementById ('table') && document.getElementById ('editor')) {
        document.getElementById ('table').style.top = 'calc(100vh - 5px)';
        document.getElementById ('editor').style.height = 'calc(100vh - 5px)';
        editor.resize ();

        document.getElementById ('tableAppend').style.bottom = '20px';

        if (document.querySelector ('svg.fa-chevron-down.fr')) {
            document.querySelector ('svg.fa-chevron-down.fr').classList.add ('iconHidden');
        }
        if (document.querySelector ('svg.fa-chevron-up.fr')) {
            document.querySelector ('svg.fa-chevron-up.fr').classList.remove ('iconHidden');
        }
    }
}; setTimeout (() => hideTableComponent (), 1000);

var showTableComponent = () => {
    if (document.getElementById ('table') && document.getElementById ('editor')) {
        document.getElementById ('table').style.top = 'calc(50vh - 5px)';
        document.getElementById ('editor').style.height = 'calc(50vh - 5px)';
        editor.resize ();

        document.getElementById ('tableAppend').style.bottom  = 'calc(50vh + 20px)';

        if (document.querySelector ('svg.fa-chevron-down.fr')) {
            document.querySelector ('svg.fa-chevron-down.fr').classList.remove ('iconHidden');
        }
        if (document.querySelector ('svg.fa-chevron-up.fr')) {
            document.querySelector ('svg.fa-chevron-up.fr').classList.add ('iconHidden');
        }
    }
}

var setFocus = () => {
    if (document.getElementById ('editor')) {
        const n = editor.getSession ().getValue ().split ("\n").length;
        
        editor.focus ();
        editor.gotoLine (n);
        editor.navigateLineEnd ();

        hideTableComponent ();
    }
}; setFocus ();

var autoFormat = () => {
    editor.setValue (
        sqlFormatter.format (
            editor.getSession ().getValue (),
            {
                language    : 'spark',  // Defaults to "sql" (see the above list of supported dialects)
                indent      : '    ',   // Defaults to two spaces
                uppercase   : true,     // Defaults to false (not safe to use when SQL dialect has case-sensitive identifiers)
                linesBetweenQueries: 2, // Defaults to 1
            }
        )
    );
    editor.getSession ().setUndoManager (
        editor.getSession ().getUndoManager ());
    setFocus ();
}

var getSelectionText = () => {
    
    let selectionRange = editor.getSelectionRange (); // editor.session.getTextRange (selectionRange)
    let startLine = Number (selectionRange.start.row);
    let endLine = Number (selectionRange.end.row);

    let fullContent = '';

    for (let i = startLine; i < endLine + 1; i++) {

        fullContent += `${
            !editor.session.getLine (i).trim ().startsWith ('--')
                ? editor.session.getLine (i)
                : ''
            }${
                i < endLine + 1
                    ? ' '
                    : ''
            }`;
    }

    return fullContent;
}

NodeList.prototype.has = function (selector) {
    return Array.from (this).filter (e => e.querySelector (selector));
}

var tableArray = [
    {
        COLUNA: 'Não há valores a serem visualizados'
    }
];

const {
    LinearProgress
} = MaterialUI;