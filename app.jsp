<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8" isELIgnored ="false"%>
<%@ page import="br.com.sankhya.modelcore.auth.AuthenticationInfo" %>
<%@ taglib prefix="snk" uri="/WEB-INF/tld/sankhyaUtil.tld" %>

<!DOCTYPE html>
<html lang="en-US">

    <head>
		<snk:load />
        <meta charset="UTF-8" />
        <title></title>

        <!-- CSS -->
        <link href="${BASE_FOLDER}/css/perfect-scrollbar.css" rel="stylesheet" />
        <link href="${BASE_FOLDER}/css/prism.css" rel="stylesheet" />
        <link href="${BASE_FOLDER}/css/additional.css" rel="stylesheet" />
        <link href="${BASE_FOLDER}/css/main.less" rel="stylesheet/less" data-env="production" />

        <!-- Styles -->
        <style> @font-face { font-family: beon; src: url('${BASE_FOLDER}/css/font/beon.ttf'); } </style>
        <style> @font-face { font-family: rajdhani; src: url('${BASE_FOLDER}/css/font/rajdhani.ttf'); } </style>

        <!-- React -->
        <!-- <script defer src="${BASE_FOLDER}/js/utils/react.js"></script> -->
        <script defer src="${BASE_FOLDER}/js/utils/react.min.js"></script>
        <!-- <script defer src="${BASE_FOLDER}/js/utils/react-dom.js"></script> -->
        <script defer src="${BASE_FOLDER}/js/utils/react-dom.min.js"></script>
        <script defer src="${BASE_FOLDER}/js/utils/material-ui.min.js"></script>

        <!-- Utilities -->
        <script src="${BASE_FOLDER}/js/utils/font-awesome.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/sql-formatter.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/sweetalert.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/babel.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/perfect-scrollbar.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/less.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/prism.min.js"></script>
        <script src="${BASE_FOLDER}/js/utils/ace/ace.js"></script>

        <!-- Internals -->
        <script src="${BASE_FOLDER}/js/utils/jxutils.prod.js"></script>
        <script defer src="${BASE_FOLDER}/js/main.js"></script>
        <script defer>
            const open  = openLevel;
            const query = (new QueryJX);
            const host  = (new HostJX);
            host.removeFrame ({ instance: 'JX SQL EDITOR' });

            const globalLocal   = '${BASE_FOLDER}'; // Static Dependencies (DO NOT MODIFY!)
            const globalFullUrl = host.getURL () + '/mge/${BASE_FOLDER}';
            const userID        = '<%= ((AuthenticationInfo) session.getAttribute ("usuarioLogado")).getUserID ().toString () %>';
        </script>
        <script defer src="${BASE_FOLDER}/js/utils/dbprocessor.js"></script>


        <!-- React Components -->
        <script defer src="${BASE_FOLDER}/components/table/trow.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/table/tbody.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/table/thead.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/table/tappend.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/table/table.jsx" type="text/babel"></script>
        
        <script defer src="${BASE_FOLDER}/components/lateralBar/historic/editorHeader.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/lateralBar/historic/editorViewer.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/lateralBar/tab/barController.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/lateralBar/tab/barTab.jsx" type="text/babel"></script>
        <script defer src="${BASE_FOLDER}/components/lateralBar/lateralBar.jsx" type="text/babel"></script>

    </head>

    <body>

        <div id='editor'></div>

        <div id='tableTop'></div>
        <div id='table'></div>

    </body>

</html>