SELECT * 
FROM (
    SELECT DISTINCT
        table_name
        
    FROM
        all_tab_cols -- Tabelas indexadora de entidades
        
    WHERE
        (
            REGEXP_LIKE (TRIM (table_name), '^AD_') -- Tabelas Adicinais
        )
        AND owner NOT IN ('SYS', 'LBACSYS', 'CTXSYS', 'GSMADMIN_INTERNAL', 'MDSYS', 'OLAPSYS', 'ORDDATA', 'ORDSYS', 'SYSTEM', 'XDB', 'WMSYS', 'ORDSYS') -- Dominios padroes de tabelas
        -- Limite para nao estourar a response (Oracle 12+)
        --FETCH FIRST %limit ROWS ONLY
        
    ORDER BY
        table_name
)
    
WHERE
    -- Limite para nao estourar a response (Oracle 12-)
    ROWNUM < %limit