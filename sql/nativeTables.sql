SELECT * 
FROM (
    SELECT DISTINCT
        cols.table_name
        
    FROM
        all_tab_cols cols -- Tabelas indexadora de entidades
        
    WHERE
        (
            table_name LIKE 'T%' -- Tabelas nativas iniciadas com a letra T
            OR table_name LIKE 'V%' -- Views
            AND table_name NOT LIKE 'AD_%' -- Tabelas Adicinais
            AND table_name NOT LIKE 'ACT_%' -- Tabelas de logs de acoes
        )
        AND REGEXP_LIKE (table_name, '^[^_]+$') -- Tabelas com nome em conjuncao (Underline _)
        AND owner NOT IN ('SYS', 'LBACSYS', 'CTXSYS', 'GSMADMIN_INTERNAL', 'MDSYS', 'OLAPSYS', 'ORDDATA', 'ORDSYS', 'SYSTEM', 'XDB', 'WMSYS', 'ORDSYS') -- Dominios padroes de tabelas
        
    ORDER BY
        table_name
        
    -- Limite para nao estourar a response (Oracle 12+)
    --FETCH FIRST %limit ROWS ONLY
)
    
WHERE
    -- Limite para nao estourar a response (Oracle 12-)
    ROWNUM < %limit